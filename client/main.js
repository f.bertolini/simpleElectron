import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
	var status = Meteor.status()
	if(!status.connected){
		if(Meteor.isDesktop){
			Desktop.send('systemNotifications', 'notify', {
				title: 'Errore',
				text: 'Connessione Assente',
				icon: 'https://crate.extracog.io/staging/images/images.png',
				data: {
					someVar: 'My Data',
				},
			});
		}
	}

});

Template.hello.helpers({
  counter() {
	console.log("Get Counter");
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click .clickme'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
    if(Meteor.isDesktop){
    	Desktop.send('systemNotifications', 'notify', {
    		title: 'Title',
    		text: 'Counter Value:'+instance.counter.get(),
    		icon: 'https://crate.extracog.io/staging/images/images.png',
    		data: {
    		    someVar: 'My Data',
    		},
    	});
    }
  }
});

Template.info.events({
    'click .link'(event, instance){
        console.log("Clicked");
        Meteor.call('setId', {id: 123}, function(err, res){
          console.log(arguments);
        });
    },
    'click .restart'(event, instance){
        Desktop.send('desktop', 'restartApp');
    },
    'click .close'(event, instance){
        Desktop.send('desktop', 'closeApp');
    },
    'click .dev'(event, instance){
        Desktop.send('desktop', 'openDevTools');
    }

});
