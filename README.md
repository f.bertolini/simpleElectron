#### Simple meteor project to test Meteor-Desktop plugin #####

https://github.com/wojtkowiak/meteor-desktop

* run server-side: RUN_SERVER

(do the following only after server run)

* create electron folder if not exist: INIT_DESKTOP

* build distributable: BUILD_DESKTOP

For usage read meteor-desktop documentation.